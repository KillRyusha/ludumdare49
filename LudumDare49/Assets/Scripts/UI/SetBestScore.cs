using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetBestScore : MonoBehaviour
{
    [SerializeField] private TMP_Text bestScoreText;
    void Start()
    {
        bestScoreText.text = "Best score: " + PlayerPrefs.GetInt("BestScore"); 
    }
}
