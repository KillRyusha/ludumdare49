﻿public interface IPlayerActivator:IGameService
{
    void RestoreHP();
    void ActivatePlayer();
}