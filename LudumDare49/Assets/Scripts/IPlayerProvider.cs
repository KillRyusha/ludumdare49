﻿using UnityEngine;

public interface IPlayerProvider : IGameService
{
    GameObject GetPlayer { get; }
}
