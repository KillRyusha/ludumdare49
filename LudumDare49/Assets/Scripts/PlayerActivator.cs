﻿using UnityEngine;

public class PlayerActivator : IPlayerActivator
{
    private GameObject _player;
    private ServiceLocator _locator = ServiceLocator.Container;

    public PlayerActivator(GameObject player)
    {
        _player = player;
    }

    public void ActivatePlayer()
    {
        _player.SetActive(true);
    }
    public void RestoreHP()
    {
        _locator.Get<IPlayerHpChanger>().RestoreHP();
    }
}
