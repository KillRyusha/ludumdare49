﻿using System;

public interface ISlotAnimationStarter :IGameService
{
    void StartSlotAnimation(float timeForAnimationEnd, PlayerAttackTypes playerAttackTypes, Action OnAnimationEnd = null);
    void LoadAnimationData();
}