﻿using UnityEngine;

public class PlayerAttackImageType : MonoBehaviour
{
    [SerializeField] private GameObject playerAttackImage;
    [SerializeField] private PlayerAttackTypes playerAttackType;

    public GameObject GetPlayerAttackImage => playerAttackImage;
    public PlayerAttackTypes GetPlayerAttackType => playerAttackType;
}
