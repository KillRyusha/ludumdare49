﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotRandomAnimatior : MonoBehaviour, ISlotAnimationStarter
{
    [SerializeField] private int AMOUNT_OF_MOVES = 20;
    [SerializeField] private int amountOfLastMoves = 20;
    [SerializeField] private int amountOfSpins = 5;
    [SerializeField] private int amountOfsprites = 5;
    [SerializeField] private float spriteHieght = 100;
    [SerializeField] private GameObject _locationSlots;
    [SerializeField] private GameObject _attackLocationSlots;
    [SerializeField] private Transform localStartPosition;
    [SerializeField] private float waitTime;
    [SerializeField] private GameObject[] images;
    [SerializeField] private GameObject[] attackImages;
    [SerializeField] private Image[] coverImages;

    [Header("Штуки для анимации DoTween")]
    [SerializeField] private Transform[] mainPositions;
    [SerializeField] private Image backGround;
    [SerializeField] private Transform randomizerTransform;
    [SerializeField] private Camera cam;
    [SerializeField] private float camSize;
    [SerializeField] private float camBaseSize;
    [SerializeField] private Color[] backGroundColors;
    [SerializeField] private Color[] coverBackGroundColors;
    private PlayerAttackTypes _playerAttackType;

    private Dictionary<int, GameObject> _slotAttackImages = new Dictionary<int, GameObject>();
    private Dictionary<PlayerAttackTypes, int> _slotAttackOrder = new Dictionary<PlayerAttackTypes, int>();
    private Dictionary<int, GameObject> _slotImages = new Dictionary<int, GameObject>();
    private Dictionary<RoomTypes, int> _slotOrder = new Dictionary<RoomTypes, int>();
    private int _amountOfActiveImagesAttack;
    private int _amountOfActiveImages;
    private ServiceLocator _locator = ServiceLocator.Container;
    public void StartSlotAnimation(float timeForAnimationEnd,PlayerAttackTypes playerAttackTypes, Action OnAnimationEnd=null)
    {
        StartCoroutine(SpinLocationSlot(timeForAnimationEnd, camSize,_playerAttackType, ()=> OnAnimationEnd?.Invoke()));
    }

    public void LoadAnimationData()
    {
        _slotAttackImages.Clear();
        _slotImages.Clear();
        _slotAttackOrder.Clear();
        _slotOrder.Clear();
        for (int i = 0; i < images.Length; i++)
        {
            _slotImages.Add(i, images[i]);
        }
        for (int i = 0; i < attackImages.Length; i++)
        {
            _slotAttackImages.Add(i, attackImages[i]);
        }
        List<RoomTypes> roomTypes = _locator.Get<IProvideRandomRoomType>().GetAllRoomTypes;
        List<PlayerAttackTypes> attackTypes = _locator.Get<IProvideRandomAttackType>().GetAttackTypes;
        for (int i = 0; i < images.Length; i++)
        {
            if (roomTypes.Contains((RoomTypes)i))
            {
                _slotImages[i].SetActive(true);
                _slotOrder.Add((RoomTypes)i, _amountOfActiveImages);
                _amountOfActiveImages++;
            }
        }
        for(int i = 0; i < attackImages.Length; i++)
        {
            if (attackTypes.Contains((PlayerAttackTypes)i))
            {
                _slotAttackImages[i].SetActive(true);
                _slotAttackOrder.Add((PlayerAttackTypes)i, _amountOfActiveImagesAttack);
                _amountOfActiveImagesAttack++;
            }
        }
    }
    private IEnumerator SpinLocationSlot(float timeForAnimationEnd,float camSize,PlayerAttackTypes playerAttackTypes, Action OnSpinEnd =null)
    {
        randomizerTransform.DOMove(mainPositions[0].position, timeForAnimationEnd);
        cam.DOOrthoSize(camSize, timeForAnimationEnd);
        backGround.DOColor(backGroundColors[0], timeForAnimationEnd);
        for (int i = 0; i < coverImages.Length; i++)
        {
            coverImages[i].DOColor(coverBackGroundColors[i], timeForAnimationEnd);
        }
        yield return new WaitForSeconds(timeForAnimationEnd);


        Vector2 attackStartPosition = new Vector2(_attackLocationSlots.transform.position.x, localStartPosition.position.y + _slotAttackOrder.Count * spriteHieght);
        Vector2 startPosition = new Vector2(_locationSlots.transform.position.x, localStartPosition.position.y + _slotOrder.Count * spriteHieght);
        _locationSlots.transform.position = startPosition;
        _attackLocationSlots.transform.position = attackStartPosition;
        int repiats = 0;


        while (repiats < amountOfSpins)
        {
            float spinSpeed = spriteHieght * _slotOrder.Count / AMOUNT_OF_MOVES / repiats;
            float attackSpinSpeed = spriteHieght * _slotAttackOrder.Count / AMOUNT_OF_MOVES / repiats;
            for (int i = 0; i < AMOUNT_OF_MOVES * repiats; i++)
            {
                _locationSlots.transform.Translate(new Vector2(0, -spinSpeed));
                _attackLocationSlots.transform.Translate(new Vector2(0, -attackSpinSpeed));
                yield return new WaitForSeconds(waitTime);
            }

            _attackLocationSlots.transform.position = attackStartPosition;
            _locationSlots.transform.position = startPosition;
            repiats++;
        }
        float newAttackPosition = spriteHieght * (_slotAttackOrder[playerAttackTypes] - _amountOfActiveImagesAttack) / amountOfLastMoves / amountOfSpins;
        float newSpinSpeed = spriteHieght * (_slotOrder[_locator.Get<IProvideRandomRoomType>().GetRoomType] - _amountOfActiveImages) / amountOfLastMoves / amountOfSpins;
        for (int i = 0; i < amountOfLastMoves * amountOfSpins; i++)
        {
            _attackLocationSlots.transform.Translate(new Vector2(0, newAttackPosition));
            _locationSlots.transform.Translate(new Vector2(0, newSpinSpeed));
            yield return new WaitForSeconds(waitTime);
        }

        randomizerTransform.DOMove(mainPositions[1].position, timeForAnimationEnd);
        cam.DOOrthoSize(camBaseSize, timeForAnimationEnd);
        backGround.DOColor(backGroundColors[1], timeForAnimationEnd);
        for (int i = 0; i < coverImages.Length; i++)
        {
            coverImages[i].DOColor(backGroundColors[1], timeForAnimationEnd);
        }
        yield return new WaitForSeconds(timeForAnimationEnd/8);
        OnSpinEnd?.Invoke();
    }
}
