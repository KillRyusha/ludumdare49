﻿using UnityEngine;

public class PlayerAttackTypeView :MonoBehaviour, IChangePlayerAttackTmage
{
    [SerializeField] private PlayerAttackImageType[] playerAttackImageTypes;

    private GameObject _previousPlayerAttackImage;

    public void ChangePlayerAttackImage(PlayerAttackTypes type)
    {
        _previousPlayerAttackImage?.SetActive(false);
        for (int i = 0; i < playerAttackImageTypes.Length; i++)
        {
            if (playerAttackImageTypes[i].GetPlayerAttackType == type)
            {
                playerAttackImageTypes[i].GetPlayerAttackImage.SetActive(true);
                _previousPlayerAttackImage = playerAttackImageTypes[i].GetPlayerAttackImage;
                return;
            }
        }
    }
}
