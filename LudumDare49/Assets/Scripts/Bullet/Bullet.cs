﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float _speed;
    private Transform _startPosition;
    private float _directionX;
    private float _maxDistance;
    private void Start()
    {
        gameObject.SetActive(false);
    }
    private void Update()
    {
        if (transform.position.x < _startPosition.position.x + _maxDistance && transform.position.x > -(_startPosition.position.x + _maxDistance))
            transform.Translate(Vector2.right * _directionX * _speed * Time.deltaTime);
        else
            gameObject.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground" || collision.transform.GetComponent<EnemyDamageReceiver>())
            gameObject.SetActive(false);
    }

    public void Shoot(float speed, float maxDistance, float directionX, Transform shotPosition)
    {
        gameObject.SetActive(true);
        transform.position = shotPosition.position;
        _directionX = directionX;
        _maxDistance = maxDistance;
        _startPosition = shotPosition;
        _speed = speed;

        //if (directionX < 0)
        //    transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
    }
}
