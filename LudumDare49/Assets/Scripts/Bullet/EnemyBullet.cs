﻿using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    private float _speed;
    private Transform _startPosition;
    private Transform _toPosition;
    private float _directionX;
    private float _distance;
    private float _maxDistance;
    private void Start()
    {
        gameObject.SetActive(false);
    }
    private void Update()
    {
        if (transform.position.x < _startPosition.position.x + _maxDistance && transform.position.x > -(_startPosition.position.x + _maxDistance) || transform.position.x == _startPosition.position.x)
            transform.Translate(Vector2.right * _directionX * _speed * Time.deltaTime);
        else
            gameObject.SetActive(false);

        if (_startPosition.position.x > _toPosition.position.x)
            _directionX = -1;
        else if (_startPosition.position.x < _toPosition.position.x)
            _directionX = 1;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground" || collision.transform.GetComponent<PlayerDamageReceiver>())
            gameObject.SetActive(false);
    }

    public void Shoot(float speed, float maxDistance, Transform directionX, Transform shotPosition)
    {
        gameObject.SetActive(true);
        transform.position = shotPosition.position;
        _toPosition = directionX;
        _maxDistance = maxDistance;
        _startPosition = shotPosition;
        _speed = speed;
    }
}