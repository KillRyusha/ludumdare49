﻿using UnityEngine;

public class PlayerDisactivator : IPlayerDisactivator
{
    private GameObject _player;

    public PlayerDisactivator(GameObject player)
    {
        _player = player;
    }

    public void DisactivatePlayer()
    {
        _player.SetActive(false);
    }
}
