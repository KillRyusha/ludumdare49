﻿using UnityEngine;

public class RoomActivator : IActivateNewRoom,IDisactivateRoom
{
    private RoomsStorage _roomsStorage;
    private ServiceLocator _locator = ServiceLocator.Container;

    public RoomActivator(RoomsStorage roomsStorage)
    {
        _roomsStorage = roomsStorage;
    }

    public void ActivateNewRoomByType(RoomTypes type)
    {
        GameObject[] enemies = _roomsStorage.GetCurrentRoom?.transform.GetComponent<Room>().GetEnemies;
        for (int i = 0; i < enemies?.Length; i++)
        {
            enemies[i]?.SetActive(true);
        }
        _roomsStorage.GetCurrentRoom?.SetActive(false);
        _roomsStorage.GetRoomByType(type).SetActive(true);
    }

    public void DiactivateCurrentRoom()
    {
        _roomsStorage.GetCurrentRoom?.SetActive(false);
    }
}
