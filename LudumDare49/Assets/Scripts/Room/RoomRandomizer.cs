﻿using System.Collections.Generic;
using UnityEngine;

public class RoomRandomizer : ISetRandomRoomTypes, IProvideRandomRoomType
{
    private List<RoomTypes> _roomTypes = new List<RoomTypes>();
    private RoomTypes _currentRoomType;
    public void AddRoomTypes(RoomTypes roomType)
    {
        if(!_roomTypes.Contains(roomType))
        _roomTypes.Add(roomType);
    }
    public void SetRandomRoom()
    {
        _currentRoomType = _roomTypes[Random.Range(0, _roomTypes.Count)];
    }
    public List<RoomTypes> GetAllRoomTypes => _roomTypes;
    public RoomTypes GetRoomType => _currentRoomType;
}
