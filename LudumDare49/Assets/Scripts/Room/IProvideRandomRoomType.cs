﻿using System.Collections.Generic;

public interface IProvideRandomRoomType : IGameService
{
    List<RoomTypes> GetAllRoomTypes { get; }
    RoomTypes GetRoomType { get; }
}
