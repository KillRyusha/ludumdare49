﻿using UnityEngine;

public class Room : MonoBehaviour
{
    [SerializeField] private GameObject room;
    [SerializeField] private PlayerSpawnPositions spawnPositions;
    [SerializeField] private GameObject[] enemies;

    public GameObject[] GetEnemies => enemies;
    public GameObject GetRoom => room;
    public PlayerSpawnPositions GetSpawnPositions=> spawnPositions;
}
