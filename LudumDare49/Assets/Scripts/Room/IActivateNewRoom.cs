﻿public interface IActivateNewRoom :IGameService
{
    void ActivateNewRoomByType(RoomTypes type);
}
