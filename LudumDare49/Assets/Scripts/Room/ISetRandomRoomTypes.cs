﻿public interface ISetRandomRoomTypes : IGameService
{
    void SetRandomRoom();
    void AddRoomTypes(RoomTypes roomType);
}
