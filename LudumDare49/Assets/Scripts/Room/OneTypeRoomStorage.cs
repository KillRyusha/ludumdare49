﻿using System.Collections.Generic;
using UnityEngine;

public class OneTypeRoomStorage : MonoBehaviour
{
    [SerializeField] private RoomTypes roomType;
    [SerializeField] private Room[] rooms;
    private PlayerSpawnPositions[] _positions;
    public Dictionary<RoomTypes, GameObject[]> RoomByType = new Dictionary<RoomTypes, GameObject[]>();

    private void Awake()
    {
        GameObject[] roomsGameObjects = new GameObject[rooms.Length];
        _positions = new PlayerSpawnPositions[rooms.Length];
        for (int i = 0; i < roomsGameObjects.Length; i++)
        {
            roomsGameObjects[i] = rooms[i].GetRoom;
            _positions[i] = rooms[i].GetSpawnPositions;
        }
        RoomByType.Add(roomType, roomsGameObjects);
    }
    public PlayerSpawnPositions[] GetSpawnPositions => _positions;
    public RoomTypes GetRoomType => roomType;
}
