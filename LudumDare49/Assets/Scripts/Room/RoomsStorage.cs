﻿using UnityEngine;

public class RoomsStorage : MonoBehaviour
{
    [SerializeField] private OneTypeRoomStorage[] typeRoomStorages;
    private GameObject _currentRoom;

    public GameObject GetCurrentRoom => _currentRoom;

    public GameObject GetRoomByType(RoomTypes roomType)
    {   
        OneTypeRoomStorage rooms = typeRoomStorages[(int)roomType];
        Debug.Log(rooms);
        GameObject[] roomsGameObjects = rooms.RoomByType[rooms.GetRoomType];
        _currentRoom = roomsGameObjects[Random.Range(0, roomsGameObjects.Length)];
        return _currentRoom;
    }

    public Transform GetSpawnPosition()
    {
        Transform[] roomSpawnPositions = _currentRoom.transform.GetComponent<Room>().GetSpawnPositions.GetSpawnPositions;
        Transform spawnPosition = roomSpawnPositions[Random.Range(0, roomSpawnPositions.Length)];
        return spawnPosition;
    }
}
