﻿using UnityEngine;

public class UISetActive : MonoBehaviour, IUIDisactivator,IUIActivator
{
    [SerializeField] private GameObject[] uiTosetActive;

    public void DisactivateUI()
    {
        for (int i = 0; i < uiTosetActive.Length; i++)
        {
            uiTosetActive[i].SetActive(false);
        }
    }

    public void ActivateUI()
    {
        for (int i = 0; i < uiTosetActive.Length; i++)
        {
            uiTosetActive[i].SetActive(true);
        }
    }
}
