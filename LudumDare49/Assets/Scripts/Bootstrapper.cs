﻿using UnityEngine;
using UnityEngine.UI;

public class Bootstrapper : MonoBehaviour,ICoroutineRunner
{
    [SerializeField] private GameObject player;
    [SerializeField] private PlayerHP playerHP;
    [SerializeField] private Transform playerPosition;
    [SerializeField] private PlayerScore playerScore;
    [SerializeField] private RoomsStorage roomsStorage;
    [SerializeField] private RoomTypes[] roomTypes;
    [SerializeField] private PlayerAttackTypes[] attackTypes;
    [SerializeField] private float waitBeforSpinEnd;
    [SerializeField] private  int minWaitTime;
    [SerializeField] private int maxWaitTime;
    [SerializeField] private RoomTypeView roomTypeView;
    [SerializeField] private PlayerAttackTypeView playerAttackImageType;
    [SerializeField] private SlotRandomAnimatior slotRandomAnimatior;
    [SerializeField] private UISetActive uISetActive;

    private static ServiceLocator _locator = ServiceLocator.Container;
    private bool isGameSpoted;
    public void Awake()
    {
        RegisterServices();
        playerScore.ChangeScore(0);
    }
    public void Start()
    {
        //LoadTypes();
    }

    public void Update()
    {
    }
    public void StartGame()
    {
        StopAllCoroutines();
        LoadTypes();
        SetUpRandom();
        _locator.Get<IUIDisactivator>().DisactivateUI();
        _locator.Get<IPlayerActivator>().RestoreHP();
        _locator.Get<IChangePlayerAttactType>().ChangeAttackTypeTo(_locator.Get<IProvideRandomAttackType>().GetAttackType);
        _locator.Get<ISlotAnimationStarter>().StartSlotAnimation(waitBeforSpinEnd, _locator.Get<IProvideRandomAttackType>().GetAttackType,() =>
        {
            _locator.Get<IDisactivateRoom>().DiactivateCurrentRoom();
            _locator.Get<IPlayerDisactivator>().DisactivatePlayer();
            RoomSetUp();
            _locator.Get<IUIActivator>().ActivateUI();
            _locator.Get<IPlayerActivator>().ActivatePlayer();
            RunGameplay();
        });
    }

    private void RoomSetUp()
    {
        RoomTypes type = _locator.Get<IProvideRandomRoomType>().GetRoomType;
        _locator.Get<IActivateNewRoom>().ActivateNewRoomByType(type);
        _locator.Get<ISetPlayerPosition>().SetPlayerPosition(playerPosition);
        // _locator.Get<IChangePlayerAttackTmage>().ChangePlayerAttackImage(_locator.Get<IProvideRandomAttackType>().GetAttackType);
        // _locator.Get<IChangeRoomImage>().ChangeRoomImage(type);
    }
    public void SetUpRandom()
    {
        _locator.Get<ISetRandomRoomTypes>().SetRandomRoom();
        _locator.Get<IChangePlayerAttactType>().ChangePlayerAttactType();
        _locator.Get<ISlotAnimationStarter>().LoadAnimationData();
    }
    public void LoadTypes()
    {
        for (int i = 0; i < roomTypes.Length; i++)
            _locator.Get<ISetRandomRoomTypes>().AddRoomTypes(roomTypes[i]);
        for (int i = 0; i < attackTypes.Length; i++)
            _locator.Get<ISetRandomAttackTypes>().AddAttackTypes(attackTypes[i]);
    }
    public void RunGameplay()
    {
        _locator.Get<IPlayerTeleportTimer>().StartTeleportTimer(minWaitTime, maxWaitTime, () =>
        {
            SetUpRandom();
            _locator.Get<IChangePlayerAttactType>().ChangeAttackTypeTo(_locator.Get<IProvideRandomAttackType>().GetAttackType);
            _locator.Get<IUIDisactivator>().DisactivateUI();
            _locator.Get<IDisactivateRoom>().DiactivateCurrentRoom();
            _locator.Get<IPlayerDisactivator>().DisactivatePlayer();
            _locator.Get<ISlotAnimationStarter>().StartSlotAnimation(waitBeforSpinEnd, _locator.Get<IProvideRandomAttackType>().GetAttackType,() =>
            {
                RoomSetUp();
                _locator.Get<IUIActivator>().ActivateUI();
                _locator.Get<IPlayerActivator>().ActivatePlayer();
                RunGameplay();
            });
        });
    }

   // private void PlayerTeletort()
   // {
   //     _locator.Get<IPlayerTeleportTimer>().StartTeleportTimer(minWaitTime, maxWaitTime, () =>
   //     {
   //       // _locator.Get<ISlotAnimationStarter>().StartSlotAnimation(()=> _locator.Get<IPlayerTeleporter>().TeleportPlayer());
   //     });
   // }

    private void RegisterServices()
    {
        RoomRandomizer roomRandomizer = new RoomRandomizer();
        _locator.RegisterSingle<IPlayerHpProvider>(playerHP);
        _locator.RegisterSingle<IPlayerHpChanger>(playerHP);
        _locator.RegisterSingle<IPlayerScoreProvider>(playerScore);
        _locator.RegisterSingle<IPlayerScoreChange>(playerScore);
        RoomActivator roomActivator = new RoomActivator(roomsStorage);
        _locator.RegisterSingle<IActivateNewRoom>(roomActivator);
        _locator.RegisterSingle<IDisactivateRoom>(roomActivator);
        _locator.RegisterSingle<IProvideRandomRoomType>(roomRandomizer);
        _locator.RegisterSingle<ISetRandomRoomTypes>(roomRandomizer);
        _locator.RegisterSingle<ISetPlayerPosition>(new PlayerPositionSetter(roomsStorage));
        _locator.RegisterSingle<ITimer>(new TimerService(this));
        _locator.RegisterSingle<IPlayerTeleportTimer>(new PlayerTeleportTimer(this));
        _locator.RegisterSingle<IPlayerTeleporter>(new PlayerTeleporter(playerPosition));
        PlayerAttackRandomizer playerAttackRandomizer = new PlayerAttackRandomizer();
        _locator.RegisterSingle<IProvideRandomAttackType>(playerAttackRandomizer);
        _locator.RegisterSingle<ISetRandomAttackTypes>(playerAttackRandomizer);
        _locator.RegisterSingle<IChangeRoomImage>(roomTypeView);
        _locator.RegisterSingle<IChangePlayerAttackTmage>(playerAttackImageType);
        _locator.RegisterSingle<IChangePlayerAttactType>(playerAttackRandomizer);
        _locator.RegisterSingle<ISlotAnimationStarter>(slotRandomAnimatior);
        _locator.RegisterSingle<IPlayerDisactivator>(new PlayerDisactivator(player));
        _locator.RegisterSingle<IPlayerActivator>(new PlayerActivator(player));
        _locator.RegisterSingle<IUIDisactivator>(uISetActive);
        _locator.RegisterSingle<IUIActivator>(uISetActive);
        _locator.RegisterSingle<IPlayerProvider>(new PlayerProvider(player));
    }
}
