﻿using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    [SerializeField] private PlayerHP playerHP;
    [SerializeField] private GameObject loseWindow;
    private ServiceLocator _locator = ServiceLocator.Container;
    private void OnEnable()
    {
        playerHP.OnHPChanged +=ShowLoseWindow;
        playerHP.OnHPChanged += HidePlayer;
    }

    private void OnDisable()
    {
        playerHP.OnHPChanged -= ShowLoseWindow;
        playerHP.OnHPChanged -= HidePlayer;
    }

    private void ShowLoseWindow()
    {
        if (_locator.Get<IPlayerHpProvider>().GetHP <= 0)
            loseWindow.SetActive(true);
    }
    private void HidePlayer()
    {
        if (_locator.Get<IPlayerHpProvider>().GetHP <= 0)
        {
            StopAllCoroutines();
            _locator.Get<IPlayerDisactivator>().DisactivatePlayer();
            _locator.Get<IDisactivateRoom>().DiactivateCurrentRoom();
            _locator.Get<IPlayerScoreChange>().SetBestScore();
        }
    }
}


