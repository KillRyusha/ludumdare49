﻿
using UnityEngine;

public interface ISetPlayerPosition :IGameService
{
    void SetPlayerPosition(Transform playerPosition);
}