﻿using TMPro;
using UnityEngine;

public class PlayerScoreView : MonoBehaviour
{
    [SerializeField] private PlayerScore playerScore;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private TMP_Text menuBestScore;
    private ServiceLocator _locator = ServiceLocator.Container;
    private void OnEnable()
    {
        playerScore.OnScoreChange += ChangeScoreText;
        ChangeScoreText();
    }
    private void OnDisable()
    {
        playerScore.OnScoreChange -= ChangeScoreText;
    }

    public void ChangeScoreText()
    {
        menuBestScore.text = "Best score: " + _locator.Get<IPlayerScoreProvider>().GetBestScore.ToString();
        scoreText.text = "Score: " + _locator.Get<IPlayerScoreProvider>().GetScore.ToString();
    }

    
}


