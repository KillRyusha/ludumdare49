﻿using UnityEngine;

public class PlayerMeleeAttack : AAttack
{
    private Transform _hitPosition;
    private Transform _playerTransform;
    private GameObject _hand;
    private float _handShowTime;

    public PlayerMeleeAttack(Transform hitPosition, GameObject hand, Transform playerTransform, float handShowTime)
    {
        _hitPosition = hitPosition;
        _hand = hand;
        _handShowTime = handShowTime;
        _playerTransform = playerTransform;
    }

    public override void Attack()
    {
        _hand.transform.position = _hitPosition.position;
        _hand.SetActive(true);
        ServiceLocator.Container.Get<ITimer>().StartTimer(_handShowTime, () =>
         _hand.SetActive(false));
    }
}



