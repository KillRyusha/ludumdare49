﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHpView : MonoBehaviour
{
    [SerializeField] private PlayerHP playerHP;

    [SerializeField] private Image hpBar;
    private ServiceLocator _locator = ServiceLocator.Container;
    private float _maxHp;
    private float _currentHp;

    private void OnEnable()
    {
        playerHP.OnHPChanged += ChangeHPView;
    }
    private void OnDisable()
    {
        playerHP.OnHPChanged -= ChangeHPView;
    }

    private void ChangeHPView()
    {
        _maxHp = _locator.Get<IPlayerHpProvider>().GetMaxHP;
        _currentHp = _locator.Get<IPlayerHpProvider>().GetHP;
        hpBar.fillAmount = _currentHp / _maxHp;
    }


}
