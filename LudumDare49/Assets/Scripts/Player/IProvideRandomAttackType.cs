﻿using System.Collections.Generic;

public interface IProvideRandomAttackType : IGameService
{
    List<PlayerAttackTypes> GetAttackTypes { get; }
    PlayerAttackTypes GetAttackType { get; }
}
