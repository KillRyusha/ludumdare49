﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour 
{
    [SerializeField] private Transform playerView;
    [SerializeField] private Transform shotPosition;
    [SerializeField] private BulletStorage bulletStorage;
    [SerializeField] private GameObject hand;
    [SerializeField] private float bulletDistance;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private float handShowTime;

    private ServiceLocator _locator = ServiceLocator.Container;

    private Dictionary<PlayerAttackTypes, AAttack> AttackOptions = new Dictionary<PlayerAttackTypes, AAttack>();

    private void Awake()
    {
        AttackOptions.Add(PlayerAttackTypes.RANGE, new PlayerShooter(shotPosition,bulletStorage,bulletDistance,bulletSpeed, playerView));
        AttackOptions.Add(PlayerAttackTypes.MELEE, new PlayerMeleeAttack(shotPosition,hand,playerView,handShowTime));
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            AttackOptions[_locator.Get<IProvideRandomAttackType>().GetAttackType].Attack();
        }
    }
}
