using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    private const int GROUND_LAYER = 6;
    [SerializeField] private Rigidbody2D rigidbody2D;
    [SerializeField] private Transform playerView;
    [Range(0, 0.3f)] [SerializeField] private float movementSmoothing = 0.05f;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpPower;
    [SerializeField] private float overlapRadius;
    [SerializeField] private Transform jumpCheck;
    private static readonly int IDLE = Animator.StringToHash("PlayerIdle");
    private static readonly int WALK = Animator.StringToHash("PlayerMove");
    private static readonly int JUMP = Animator.StringToHash("PlayerJump");
    private static readonly int ATTACK = Animator.StringToHash("PlayerAttack");
    private ServiceLocator _locator = ServiceLocator.Container;
    public Animator Animator;
    private Vector3 _velocity = Vector3.zero;
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A)|| Input.GetKey(KeyCode.D))
        {
            float _moveHorizontl = Input.GetAxis("Horizontal");
            Vector3 targetVelocity = new Vector2(_moveHorizontl * moveSpeed* Time.fixedDeltaTime, rigidbody2D.velocity.y);
            rigidbody2D.velocity = Vector3.SmoothDamp(rigidbody2D.velocity, targetVelocity, ref _velocity, movementSmoothing);
            if(Input.GetKey(KeyCode.A))
                playerView.localScale = new Vector3(-0.13f, 0.13f, 0.13f);
            else if(Input.GetKey(KeyCode.D))
                playerView.localScale = new Vector3(0.13f, 0.13f, 0.13f);

            if (Animator.GetBool("Jump") == false)
            {
                PlayMove();
            }
        }
        else
        {
            if (Animator.GetBool("Jump") == false)
                PlayIdle();
            rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
        }
        if (Input.GetMouseButtonDown(0))
        {
            PlayAttack();
        }
    }

    private void Update()
    {
        Collider2D collider2DOverlap = Physics2D.OverlapCircle(jumpCheck.position, overlapRadius,1<< GROUND_LAYER);
        if (collider2DOverlap && Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody2D.AddForce(Vector2.up* jumpPower, ForceMode2D.Impulse);
        }
        else if (!collider2DOverlap)
        {
            Animator.SetBool("Jump", true);
        }else if (collider2DOverlap)
        {
            Animator.SetBool("Jump", false);
        }
    }

    public void PlayIdle() =>
        Animator.Play(IDLE);
    public void PlayMove() =>
        Animator.Play(WALK);
    public void PlayJump() =>
        Animator.Play(JUMP);
    public void PlayAttack() =>
        Animator.Play(ATTACK);
}
