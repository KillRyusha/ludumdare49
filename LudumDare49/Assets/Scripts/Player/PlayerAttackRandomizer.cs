﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackRandomizer : ISetRandomAttackTypes, IProvideRandomAttackType, IChangePlayerAttactType
{
    private List<PlayerAttackTypes> _attackTypes = new List<PlayerAttackTypes>();
    private PlayerAttackTypes _playerAttackType;
    public void AddAttackTypes(PlayerAttackTypes attackType)
    {
        if(!_attackTypes.Contains(attackType))
        _attackTypes.Add(attackType);
    }
    public PlayerAttackTypes GetAttackType => _playerAttackType;
    public List<PlayerAttackTypes> GetAttackTypes => _attackTypes;
    public void ChangePlayerAttactType()
    {
        _playerAttackType = _attackTypes[Random.Range(0, _attackTypes.Count)];
    }

    public void ChangeAttackTypeTo(PlayerAttackTypes playerAttack)
    {
        _playerAttackType = playerAttack;
    }
}
