﻿public interface IPlayerHpChanger : IGameService
{
    void RestoreHP();
    void ChangeHP(float hpChange);
}
