﻿public interface IPlayerScoreChange :IGameService
{
    void SetBestScore();
    void ChangeScore(int scoreChange);
}


