﻿using UnityEngine;

public class PlayerDamageReceiver : MonoBehaviour
{

    [SerializeField] private float waitBeforeGetDamage;
    [SerializeField] private AudioSource takeDMGSound;
     private bool _canAttack=true;
    [SerializeField] private int _enemyDamage = -1;
    private ServiceLocator _locator = ServiceLocator.Container;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        EnemyAttack enemyAttack = collision.transform.GetComponent<EnemyAttack>();
        if (collision.tag=="Enemy")
        {
            takeDMGSound.Play();
                _locator.Get<IPlayerHpChanger>().ChangeHP(_enemyDamage);
         
        }
    }
}


