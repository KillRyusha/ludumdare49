﻿public interface IChangePlayerAttactType : IGameService
{
    void ChangePlayerAttactType();
    void ChangeAttackTypeTo(PlayerAttackTypes playerAttack);
}