﻿using UnityEngine;

    public class GenericInput : IInputService
    {
        private const string VERTICAL_AXIS_NAME = "Vertical";
        private const string HORIZONTAL_AXIS_NAME = "Horizontal";

        public Vector2 Direction()
        { 
           return new Vector2(Input.GetAxisRaw(HORIZONTAL_AXIS_NAME), Input.GetAxisRaw(VERTICAL_AXIS_NAME)).normalized;
        }
        
        public bool IsActionButtonPressed =>
            Input.GetKeyDown(KeyCode.E);

        public bool IsMovingRight => Direction().x > 0 ;
    }