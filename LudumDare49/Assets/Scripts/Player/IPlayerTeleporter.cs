﻿public interface IPlayerTeleporter : IGameService
{
    void TeleportPlayer();
}