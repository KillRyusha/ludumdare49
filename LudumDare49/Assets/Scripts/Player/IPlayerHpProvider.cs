﻿public interface IPlayerHpProvider : IGameService
{
    float GetHP { get; }
    float GetMaxHP { get; }
}
