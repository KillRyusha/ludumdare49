﻿using UnityEngine;

public class PlayerTeleporter :IPlayerTeleporter
{
    private Transform _playerPosition;
    private ServiceLocator _locator = ServiceLocator.Container;

    public PlayerTeleporter(Transform playerPosition)
    {
        _playerPosition = playerPosition;
    }

    public void TeleportPlayer()
    {
        RoomTypes type = _locator.Get<IProvideRandomRoomType>().GetRoomType;
        _locator.Get<IActivateNewRoom>().ActivateNewRoomByType(type);
    }
}
