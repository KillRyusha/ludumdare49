﻿public interface ISetRandomAttackTypes : IGameService
{
    void AddAttackTypes(PlayerAttackTypes attackType);
}