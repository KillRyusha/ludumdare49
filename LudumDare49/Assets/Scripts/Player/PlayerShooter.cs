using System.Collections;
using UnityEngine;

public class PlayerShooter : AAttack
{
    private Transform _shotPosition;
    private Transform _playerTransform;
    private BulletStorage _bulletStorage;
    private float _bulletDistance;
    private float _bulletSpeed;
    private int _currentBulletNumber;

    public PlayerShooter(Transform shotPosition, BulletStorage bulletStorage, float bulletDistance, float bulletSpeed, Transform playerTransform)
    {
        _shotPosition = shotPosition;
        _bulletStorage = bulletStorage;
        _bulletDistance = bulletDistance;
        _bulletSpeed = bulletSpeed;
        _playerTransform = playerTransform;
    }

    public override void Attack() 
    { 
        _bulletStorage.BulletPrefab[_currentBulletNumber].Shoot(_bulletSpeed,_bulletDistance,_playerTransform.localScale.x, _shotPosition);
        _currentBulletNumber++;
        if(_currentBulletNumber>=_bulletStorage.BulletPrefab.Length)
        {
            _currentBulletNumber = 0;
        }
    }
}

