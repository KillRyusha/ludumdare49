﻿
using UnityEngine;

public class PlayerPositionSetter : ISetPlayerPosition
{
    private RoomsStorage _storage;

    public PlayerPositionSetter(RoomsStorage storage)
    {
        _storage = storage;
    }

    public void SetPlayerPosition(Transform playerPosition)
    {
       playerPosition.position = _storage.GetSpawnPosition().position;
    }
}
