﻿using System;
using UnityEngine;

public class PlayerHP : MonoBehaviour, IPlayerHpProvider, IPlayerHpChanger
{
    public Action OnHPChanged;
    [SerializeField] private float maxHp = 100;
    private float _hp = 100;

    private void Start()
    {
        RestoreHP();
    }

    public float GetHP => _hp;
    public float GetMaxHP => maxHp;
    public void RestoreHP()
    {
        ChangeHP(maxHp);
    }
    public void ChangeHP(float hpChange)
    {
        _hp += hpChange;
        if (_hp > maxHp)
            _hp = maxHp;
        else if (_hp <= 0)
            _hp = 0;
        OnHPChanged?.Invoke();
    }
}


