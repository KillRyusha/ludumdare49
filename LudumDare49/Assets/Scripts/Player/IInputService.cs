﻿using UnityEngine;

    public interface IInputService : IGameService
    {
        Vector2 Direction();
        bool IsMovingRight { get; }
        bool IsActionButtonPressed { get; }
    }