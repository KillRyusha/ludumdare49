﻿public interface IPlayerScoreProvider :IGameService
{
    int GetBestScore { get; }
    int GetScore { get; }
}


