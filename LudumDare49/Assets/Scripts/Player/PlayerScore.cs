﻿using System;
using UnityEngine;

public class PlayerScore : MonoBehaviour,IPlayerScoreProvider, IPlayerScoreChange
{
    public Action OnScoreChange;
    private int _score;
    private int _bestScore;

    private void Start()
    {
        _score = PlayerPrefs.GetInt("Score");
        _bestScore = PlayerPrefs.GetInt("BestScore");
    }
    public void SetBestScore()
    {
        if (_bestScore <= _score)
        {
            _bestScore = _score;
            PlayerPrefs.SetInt("BestScore", _bestScore);
        }
        ChangeScore(0);
        _score = 0;
    }
    public int GetBestScore => _bestScore;
    public int GetScore => _score;
    public void ChangeScore(int scoreChange)
    {

        _score += scoreChange;
        OnScoreChange?.Invoke();
    }
}


