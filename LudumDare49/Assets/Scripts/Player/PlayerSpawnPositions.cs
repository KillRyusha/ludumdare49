﻿
using UnityEngine;

public class PlayerSpawnPositions : MonoBehaviour
{
    [SerializeField] private Transform[] spawnPositions;

    public Transform[] GetSpawnPositions => spawnPositions;
}
