﻿using UnityEngine;

public class PlayerProvider: IPlayerProvider
{
    private GameObject _player;

    public PlayerProvider(GameObject player)
    {
        _player = player;
    }

    public GameObject GetPlayer => _player;
}
