﻿using System.Collections.Generic;
using UnityEngine;

public class RoomImageType : MonoBehaviour
{
    [SerializeField] private GameObject roomImage;
    [SerializeField] private RoomTypes roomType;

    public GameObject GetRoomImage => roomImage;
    public RoomTypes GetRoomType => roomType;
}