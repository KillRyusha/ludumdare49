﻿using System;
using System.Collections;
using UnityEngine;

public class PlayerTeleportTimer : IPlayerTeleportTimer
{
    private ICoroutineRunner _coroutineRunner;

    public PlayerTeleportTimer(ICoroutineRunner coroutineRunner)
    {
        _coroutineRunner = coroutineRunner;
    }

    public void StartTeleportTimer(int minWaitTime, int maxWaitTime, Action OnTimeEnd = null)
    {
        _coroutineRunner.StartCoroutine(TeleportTimer(minWaitTime, maxWaitTime, OnTimeEnd));
    }
    private IEnumerator TeleportTimer(int minWaitTime, int maxWaitTime, Action OnTimeEnd=null)
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(minWaitTime,maxWaitTime));
        OnTimeEnd?.Invoke();
    }

}
