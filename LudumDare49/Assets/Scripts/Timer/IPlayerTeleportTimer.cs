﻿using System;

public interface IPlayerTeleportTimer : IGameService
{
    void StartTeleportTimer(int minWaitTime, int maxWaitTime, Action OnTimeEnd = null);
}
