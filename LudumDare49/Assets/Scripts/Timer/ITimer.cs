﻿using System;

public interface ITimer : IGameService
{
    void StartTimer(float time, Action onTimerEnd = null);
}

