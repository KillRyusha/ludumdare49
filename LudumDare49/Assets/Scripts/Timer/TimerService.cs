﻿using System;
using System.Collections;
using UnityEngine;

    public class TimerService : ITimer
    {
        private ICoroutineRunner _coroutineRunner;

        public TimerService(ICoroutineRunner coroutineRunner)
        {
            _coroutineRunner = coroutineRunner;
        }

        public void StartTimer(float time, Action onTimerEnd = null)
        {
            _coroutineRunner.StartCoroutine(Timer(time, onTimerEnd));
        }
        public IEnumerator Timer(float time, Action onTimeEnd = null)
        {
            yield return new WaitForSeconds(time);
            onTimeEnd?.Invoke();
        }
    }

