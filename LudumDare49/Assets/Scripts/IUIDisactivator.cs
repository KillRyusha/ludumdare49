﻿public interface IUIDisactivator : IGameService
{
    void DisactivateUI();
}