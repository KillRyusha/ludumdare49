﻿public interface IPlayerDisactivator : IGameService
{
    void DisactivatePlayer();
}