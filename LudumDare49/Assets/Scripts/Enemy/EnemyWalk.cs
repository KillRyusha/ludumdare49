﻿using UnityEngine;

public class EnemyWalk : IEnemyMove
{
    private Transform[] _movePoints;
    private float _moveSpeed;
    private Transform _transform;
    private bool _isMovingRight;

    public EnemyWalk(Transform[] movePoints, float moveSpeed, Transform transform)
    {
        _movePoints = movePoints;
        _moveSpeed = moveSpeed;
        _transform = transform;
    }

    public void Move()
    {
        if(_isMovingRight)
            _transform.Translate(Vector2.right * _moveSpeed*Time.deltaTime);
        else
            _transform.Translate(Vector2.left * _moveSpeed * Time.deltaTime);

        if(_transform.position.x > _movePoints[0].position.x&& _isMovingRight)
        {
            _isMovingRight = !_isMovingRight;
        }
        if (_transform.position.x < _movePoints[1].position.x && !_isMovingRight)
        {
            _isMovingRight = !_isMovingRight;
        }
    }
}
