﻿using UnityEngine;

public class EnemyBorder : MonoBehaviour
{
    [SerializeField] private EnemyMove enemy;
    [SerializeField] private EnemyMoveTypes enemyIdle;
    [SerializeField] private EnemyMoveTypes enemyRun;
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.GetComponent<EnemyMove>() || collision.transform.GetComponent<PlayerMovement>())
        {
            enemy.MoveTypes = enemyIdle;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.GetComponent<PlayerMovement>())
        {
            enemy.MoveTypes = enemyRun;
        }
    }
}
