﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    [SerializeField] private Transform[] movePoints;
    [SerializeField] private Transform playerPosition;
    private float moveSpeed;
    public EnemyMoveTypes MoveTypes;
    private Dictionary<EnemyMoveTypes, IEnemyMove> MoveOptions = new Dictionary<EnemyMoveTypes, IEnemyMove>();

    private void Awake()
    {
        moveSpeed = Random.Range(2f,4f);
        RegisterMoveOptions();
    }
    public void RegisterMoveOptions()
    {
        MoveOptions.Add(EnemyMoveTypes.WALK, new EnemyWalk(movePoints,moveSpeed,transform));
        MoveOptions.Add(EnemyMoveTypes.ENEMY_RUN_TO_PLAYER, new EnemyRunToPlayer(playerPosition,moveSpeed,transform));
        MoveOptions.Add(EnemyMoveTypes.STAY, new EnemyStay());
    }
    private void Update()
    {
        MoveOptions[MoveTypes].Move();
    }

}
public class EnemyStay : IEnemyMove
{
    public void Move()
    {
    }
}