﻿public interface IEnemyGetHP:IGameService
{
    int GetEnemyHP { get; }
}
