﻿using UnityEngine;

public class EnemyDamageReceiver : MonoBehaviour
{
    [SerializeField] private AudioSource hitSound;
    [SerializeField] private EnemyHp enemyHp;
    private const int PlayerDamage = 1;
    [SerializeField] private float waitBeforeGetDamage;
    private bool canAttack=true;
    private ServiceLocator _locator = ServiceLocator.Container;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.GetComponent<Bullet>() || collision.tag == "Hand")
        {
            enemyHp.ChangeEnemyHP(-PlayerDamage);
            hitSound.Play();
        }
    }
  //  private void OnTriggerStay2D(Collider2D collision)
  //  {
  //      if (collision.transform.GetComponent<Bullet>() || collision.tag == "Hand")
  //      {
  //          if (canAttack == true)
  //          {
  //              enemyHp.ChangeEnemyHP(-PlayerDamage);
  //              canAttack = false;
  //              Debug.Log("attacked");
  //          }
  //          else if (canAttack == false)
  //          {
  //              _locator.Get<ITimer>().StartTimer(waitBeforeGetDamage, () => { canAttack = true; Debug.Log(canAttack); });
  //              Debug.Log("attacked");
  //          }
  //      }
  //  }
}
