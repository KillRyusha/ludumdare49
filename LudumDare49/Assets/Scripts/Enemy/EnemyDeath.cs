﻿using UnityEngine;

public class EnemyDeath : MonoBehaviour
{
    private const int EnemyKillPoint = 1;
    [SerializeField] private AudioSource deathSound;
    [SerializeField] private GameObject enemySetUp;
    [SerializeField] private EnemyHp enemyHp;
    private ServiceLocator _locator = ServiceLocator.Container;

    private void OnEnable()
    {
        enemyHp.OnEnemyHPChanged += TurnOffEnemy;
    }
    private void OnDisable()
    {
        enemyHp.OnEnemyHPChanged -= TurnOffEnemy;
    }

    private void TurnOffEnemy()
    {
        if (enemyHp.GetEnemyHP <= 0)
        {
            deathSound.Play();
            enemySetUp.SetActive(false);
            _locator.Get<IPlayerScoreChange>().ChangeScore(EnemyKillPoint);
        }
    }
}
