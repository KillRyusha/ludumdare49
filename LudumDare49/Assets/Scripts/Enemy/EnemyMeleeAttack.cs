﻿using UnityEngine;

public class EnemyMeleeAttack : IEnemyAttack
{
    private Transform _hitPosition;
    private GameObject _hand;
    private float _handShowTime;
    private float _cooldown;


    public EnemyMeleeAttack(Transform hitPosition, GameObject hand, float handShowTime,float cooldown)
    {
        _hitPosition = hitPosition;
        _hand = hand;
        _handShowTime = handShowTime;
        _cooldown = cooldown;
    }

    public void Attack()
    {
        _hand.transform.position = _hitPosition.position;
        _hand.SetActive(true);
        ServiceLocator _locator = ServiceLocator.Container;
        _locator.Get<ITimer>().StartTimer(_handShowTime, () =>
         _hand.SetActive(false));
    }
}
