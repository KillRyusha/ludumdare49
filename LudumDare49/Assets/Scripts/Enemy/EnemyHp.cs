﻿using System;
using UnityEngine;

public class EnemyHp: MonoBehaviour, IEnemyHPChange,IEnemyGetHP
{
    public Action OnEnemyHPChanged;
    [SerializeField] private int maxHP=3;
    private int hp;
    private void OnEnable()
    {
        hp = maxHP;
    }
    public int GetEnemyHP => hp;
    public void ChangeEnemyHP(int hpChange)
    {
        hp += hpChange;
        if (hp <= 0)
            hp = 0;
        OnEnemyHPChanged?.Invoke();
    }
}
