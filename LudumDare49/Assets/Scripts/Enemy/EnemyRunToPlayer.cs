﻿using UnityEngine;

public class EnemyRunToPlayer : IEnemyMove
{
    private Transform _playerPosition;
    private float _moveSpeed;
    private Transform _transform;
    private float _transformY;

    public EnemyRunToPlayer(Transform playerPosition, float moveSpeed, Transform transform)
    {
        _playerPosition = playerPosition;
        _moveSpeed = moveSpeed;
        _transform = transform;
        _transformY = transform.position.y;
    }

    public void Move()
    {
        if (_playerPosition.position.x - _transform.position.x > 0)
        {
            _transform.position = Vector3.MoveTowards(
                _transform.position, new Vector2(_playerPosition.position.x - _playerPosition.localScale.x / 2, _transformY),
                _moveSpeed * Time.deltaTime); 
        }
        else if (_playerPosition.position.x - _transform.position.x < 0)
        {
            _transform.position = Vector3.MoveTowards(
                _transform.position, new Vector2(_playerPosition.position.x + _playerPosition.localScale.x / 2, _transformY),
                _moveSpeed * Time.deltaTime);
        }
    }
}
