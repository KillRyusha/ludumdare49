﻿public interface IEnemyHPChange : IGameService
{
    void ChangeEnemyHP(int hpChange);
}