﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private Transform _hitPosition;
    [SerializeField] private GameObject _hand;
    [SerializeField] private EnemyBulletStorage enemyBulletStorage;
    [SerializeField] private float distanse;
    [SerializeField] private float speed;
    [SerializeField] private float _handShowTime;
    [SerializeField] private float _cooldown;
    
    private bool canAttack=true;
    private ServiceLocator _locator = ServiceLocator.Container;
    public EnemyAttackTypes EnemyAttackType;
    [SerializeField] private EnemyMove _enemyMove;
    private Dictionary<EnemyAttackTypes, IEnemyAttack> EnemyAttackOptions = new Dictionary<EnemyAttackTypes, IEnemyAttack>();

    private void Awake()
    {
        EnemyAttackOptions.Add(EnemyAttackTypes.MELEE, new EnemyMeleeAttack(_hitPosition,_hand,_handShowTime, _cooldown));
        EnemyAttackOptions.Add(EnemyAttackTypes.RANGE, new EnemyRangeAttack(_hitPosition, enemyBulletStorage, distanse, speed));
    }
    private void Update()
    {
        if (_enemyMove.MoveTypes == EnemyMoveTypes.STAY && canAttack == true)
        {
            EnemyAttackOptions[EnemyAttackType].Attack();
            canAttack = false;
            _locator.Get<ITimer>().StartTimer(_cooldown, () =>
            {
                canAttack = true;
            });
            Debug.Log("Shot");
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.GetComponent<PlayerDamageReceiver>() && canAttack==true && _enemyMove.MoveTypes != EnemyMoveTypes.STAY)
        {
            EnemyAttackOptions[EnemyAttackType].Attack();
            canAttack = false;
            _locator.Get<ITimer>().StartTimer(_cooldown, () =>
            {
                canAttack = true;
            });
        }
    }
}
