﻿using UnityEngine;

public class EnemyRangeAttack : IEnemyAttack
{
    private Transform _shotPosition;
    private Transform _playerTransform;
    private EnemyBulletStorage _bulletStorage;
    private float _bulletDistance;
    private float _bulletSpeed;
    private int _currentBulletNumber;

    public EnemyRangeAttack(Transform shotPosition, EnemyBulletStorage bulletStorage, float bulletDistance, float bulletSpeed)
    {
        _shotPosition = shotPosition;
        _bulletStorage = bulletStorage;
        _bulletDistance = bulletDistance;
        _bulletSpeed = bulletSpeed;
    }

    public void Attack()
    {
        _playerTransform = ServiceLocator.Container.Get<IPlayerProvider>().GetPlayer.transform;
        _bulletStorage.BulletPrefab[_currentBulletNumber].Shoot(_bulletSpeed, _bulletDistance, _playerTransform, _shotPosition);
        _currentBulletNumber++;
        if (_currentBulletNumber >= _bulletStorage.BulletPrefab.Length)
        {
            _currentBulletNumber = 0;
        }
    }
}
