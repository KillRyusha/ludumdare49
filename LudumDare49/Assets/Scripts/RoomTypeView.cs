﻿using UnityEngine;

public class RoomTypeView : MonoBehaviour, IChangeRoomImage
{
    [SerializeField] private RoomImageType[] roomImageTypes;

    private GameObject _previousRoomImage;

    public void ChangeRoomImage(RoomTypes type)
    {
        _previousRoomImage?.SetActive(false);
        for (int i = 0; i < roomImageTypes.Length; i++)
        {
            if(roomImageTypes[i].GetRoomType == type)
            {
                roomImageTypes[i].GetRoomImage.SetActive(true);
                _previousRoomImage = roomImageTypes[i].GetRoomImage;
                return;
            }
        }
    }
}
