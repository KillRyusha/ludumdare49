﻿public interface IUIActivator : IGameService
{
    void ActivateUI();
}
